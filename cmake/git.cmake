# Check for the git executable.  If it can be found, use git to extract the
# GIT_HASH and GIT_BRANCH.  In the case of an error or if git cannot be
# found these CMake variables are set to an empty string

cmake_minimum_required(VERSION 3.10.0)

# Default values of the git hash and branch, will be overwritten if we can
# successfully extract from git

set(GIT_HASH "UNKNOWN")
set(GIT_BRANCH "UNKNOWN")

find_package(Git)
if (Git_FOUND)
    execute_process(
        COMMAND git log -1 --format=%h
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE GIT_HASH
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_QUIET
    )

    execute_process(
        COMMAND git branch --show-current
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE GIT_BRANCH
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_QUIET
    )
endif ()

# Add definitions so the GIT variables wil be available to the pre-processor
# Add definitions is being depricated
add_compile_definitions(GIT_HASH="${GIT_HASH}")
add_compile_definitions(GIT_BRANCH="${GIT_BRANCH}")
