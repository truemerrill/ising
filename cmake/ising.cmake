cmake_minimum_required(VERSION 3.10.0)

# Static library
add_library(${LIB_TARGET} STATIC
    ${SRC_PATH}/ising.c
)

target_include_directories(${LIB_TARGET} PUBLIC ${INC_PATH})
target_link_libraries(${LIB_TARGET} PUBLIC m)
set_target_properties(${LIB_TARGET} PROPERTIES PREFIX "")

# Executable
add_executable(${APP_TARGET} 
    ${SRC_PATH}/main.c
)

target_link_libraries(${APP_TARGET}
    ${LIB_TARGET}
)

target_include_directories(${APP_TARGET} PUBLIC ${INC_PATH})
