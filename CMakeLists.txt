cmake_minimum_required(VERSION 3.10)

# Set C/C++ standard
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_EXTENSIONS TRUE)

# Paths
set(APP_TARGET ising)
set(LIB_TARGET "lib${APP_TARGET}")
set(SRC_PATH ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(INC_PATH ${CMAKE_CURRENT_SOURCE_DIR}/include)

project(${APP_TARGET})
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/git.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/ising.cmake)
