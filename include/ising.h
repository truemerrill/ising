#ifndef __ISING_H__
#define __ISING_H__

#include <stddef.h>


#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
  size_t nup;         //< Index of the upper neighbor
  size_t ndown;       //< Index of the lower neighbor
  size_t nright;      //< Index of the right neighbor
  size_t nleft;       //< Index of the left neighbor
} ising_neighbors_t;


typedef struct {
  int *S;                     //< Array of spins
  ising_neighbors_t *addr;    //< Array of indices for neighbors
  size_t nrows;               //< Number of rows
  size_t ncols;               //< Number of columns
  double J;                   //< Spin-spin coupling
  double H;                   //< Magnetization
  double beta;                //< Inverse thermodynamic temperature
  int t;                      //< Time step (for metropolis algorithm)
} ising_t;


/**
 * @brief Allocate a 2D Ising model
 * 
 * The memory is dynamically allocated.  The caller is responsible for freeing
 * this memory using `ising_free`.
 * 
 * The constructor initializes all memory structures and pre-computes the 
 * indices for each spin's nearest neighbor.  The address pre-computation step
 * allows us to skip future address calculations, speeding up runtime at the 
 * cost of memory space.
 * 
 * @param nrows Number of rows in the lattice
 * @param ncols Number of columns in the lattice
 * @return ising_t* Pointer to new Ising model
 */
ising_t *ising_alloc(size_t nrows, size_t ncols);


/**
 * @brief Free a 2D Ising model
 * 
 * @param I Pointer to the Ising model
 */
void ising_free(ising_t *I);


/**
 * @brief Set the spin-spin coupling
 * 
 * @param I Pointer to the Ising model
 * @param J Coupling coefficient to set
 */
void set_coupling(ising_t *I, double J);


/**
 * @brief Set the transverse magnetization 
 * 
 * @param I Pointer to the Ising model
 * @param H Magnetization coefficient
 */
void set_magnetization(ising_t *I, double H);


/**
 * @brief Set the inverse thermodynamic temperature (beta)
 * 
 * @param I Pointer to the Ising model
 * @param beta Inverse thermodynamic temperature
 */
void set_beta(ising_t *I, double beta);


/**
 * @brief Set the temperature
 * 
 * @param I Pointer to the Ising model
 * @param kT Temperature (in units of the Boltzmann constant)
 */
void set_temperature(ising_t *I, double kT);


/**
 * @brief Set each of the spins to a random orientation
 * 
 * @param I Pointer to the Ising model
 */
void set_random_spins(ising_t *I);


/**
 * @brief Set each of the spins to value
 * 
 * @note if `value` is not +1 or -1 then the spins are not modified
 * 
 * @param I Pointer to the Ising model
 * @param value Spin value
 */
void set_spins(ising_t *I, int value);


/**
 * @brief Get the value of the (n, m)'th spin
 * 
 * @param I Pointer to the Ising model
 * @param n Row index
 * @param m Column index
 * @return double Spin value
 */
double get_spin(const ising_t *I, int n, int m);


/**
 * @brief Get the energy of the (n, m)'th spin from coupling with its neighbors 
 * 
 * @param I Pointer to the Ising model
 * @param n Row index
 * @param m Column index
 * @return double Coupling energy
 */
double get_spin_coupling_energy(const ising_t *I, int n, int m);


/**
 * @brief Get the energy of the (n, m)'th spin from magenetic field coupling
 * 
 * @param I Pointer to the Ising model
 * @param n Row index
 * @param m Column index
 * @return double Magnetization energy
 */
double get_spin_magnetization_energy(const ising_t *I, int n, int m);


/**
 * @brief Get the energy of the (n, m)'th spin
 * 
 * @param I Pointer to the Ising model
 * @param n Row index
 * @param m Column index
 * @return double Energy
 */
double get_spin_energy(const ising_t *I, int n, int m);


/**
 * @brief Get the mean spin of the Ising model
 * 
 * @param I Pointer to the Ising model
 * @return double Mean spin polarization
 */
double get_mean_spin(const ising_t *I);


/**
 * @brief Get the spin varaince of the Ising model
 * 
 * @param I Pointer to the Ising model
 * @param mean_spin Mean spin polarization
 * @return double Spin polarization variance
 */
double get_spin_variance(const ising_t *I, double mean_spin);


/**
 * @brief Get the mean energy of the Ising model
 * 
 * @param I Pointer to the Ising model
 * @return double Mean energy
 */
double get_mean_energy(const ising_t *I);


/**
 * @brief Get the energy variance of the Ising model
 * 
 * @param I Pointer to the Ising model
 * @param mean_energy Mean energy
 * @return double Energy variance
 */
double get_energy_variance(const ising_t *I, double mean_energy);


/**
 * @brief Perform one step (sweep) of the Metropolis algorithm
 * 
 * @param I Pointer to the Ising model
 */
void metropolis_step(ising_t *I);


#ifdef __cplusplus
}
#endif


#endif /* __ISING_H__ */
