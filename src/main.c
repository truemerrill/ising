#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <getopt.h>

#include "ising.h"


/* Default values, will be overwritten by command line args */
static struct settings {
  size_t nrows;
  size_t ncols;
  double J;
  double H;
  double beta;
  size_t nsteps;
  size_t nsample;
  size_t nstart;
} S = {
  .nrows = 300,
  .ncols = 300,
  .J = 1.0,
  .H = 0.0,
  .beta = 0.0,
  .nsteps = 5000,
  .nsample = 100,
  .nstart = 500
};

static bool show_header = true;
static bool show_help = false;
static bool show_version = false;

enum {
  ARG_NUM_ROWS,
  ARG_NUM_COLS,
  ARG_COUPLING,
  ARG_MAGNETIZATION,
  ARG_BETA,
  ARG_NUM_STEPS,
  ARG_SAMPLE_START,
  ARG_SAMPLE_INTERVAL,
  ARG_NO_HEADER,
  ARG_HELP,
  ARG_VERSION
};


void parse(int argc, char *argv[])
{
  int c;
  static struct option long_options[] = {
    {"num-rows", optional_argument, 0, 0},
    {"num-cols", optional_argument, 0, 0},
    {"coupling", optional_argument, 0, 0},
    {"magnetization", optional_argument, 0, 0},
    {"beta", optional_argument, 0, 0},
    {"num-steps", optional_argument, 0, 0},
    {"sample-start", optional_argument, 0, 0},
    {"sample-interval", optional_argument, 0, 0},
    {"no-header", no_argument, 0, 0},
    {"help", no_argument, 0, 0},
    {"version", no_argument, 0, 0},
    {0, 0, 0, 0}
  };

  /* getopt_long stores the option index here. */
  int option_index = 0;

  while (1) {
    c = getopt_long(argc, argv, "",
                    long_options, &option_index);
    if (c == -1) {
      break;
    }

    switch (option_index) {
      case ARG_NUM_ROWS:
        S.nrows = strtoul(optarg, NULL, 10);
        break;
      case ARG_NUM_COLS:
        S.ncols = strtoul(optarg, NULL, 10);
        break;
      case ARG_COUPLING:
        S.J = strtod(optarg, NULL);
        break;
      case ARG_MAGNETIZATION:
        S.H = strtod(optarg, NULL);
        break;
      case ARG_BETA:
        S.beta = strtod(optarg, NULL);
        break;
      case ARG_NUM_STEPS:
        S.nsteps = strtoul(optarg, NULL, 10);
        break;
      case ARG_SAMPLE_START:
        S.nstart = strtoul(optarg, NULL, 10);
        break;
      case ARG_SAMPLE_INTERVAL:
        S.nsample = strtoul(optarg, NULL, 10);
        break;
      case ARG_NO_HEADER:
        show_header = false;
        break;
      case ARG_HELP:
        show_help = true;
        break;
      case ARG_VERSION:
        show_version = true;
        break;
    }
  }
}


void print_help(void)
{
  printf("ISING - Metropolis Monte-Carlo Ising Model (%s)\n", GIT_HASH);
  printf("\n");
  printf("Usage: ising [arguments]               run simulation\n");
  printf("   or: ising [arguments] > [outfile]   redirect output to file\n");
  printf("\n");
  printf("Arguments:\n");
  printf("   --num-rows=[n]         Number of rows in the Ising model\n");
  printf("   --num-cols=[m]         Number of columns in the Ising model\n");
  printf("   --coupling=[J]         Spin-spin coupling rate\n");
  printf("   --magnetization=[h]    Magnetic field coupling rate\n");
  printf("   --beta=[b]             Inverse thermodynamic temperature\n");
  printf("   --num-steps=[N]        Number of Metropolis steps to simulate\n");
  printf("   --sample-start=[Ns]    Step which to start logging data\n");
  printf("   --sample-interval=[Ni] Number of steps between data logging\n");
  printf("   --no-header            Do not print header in output\n");
  printf("   --help                 Print help (this message) and exit\n");
  printf("   --version              Print version information and exit\n");
}


void print_header(void)
{
  printf("# ISING - Metropolis Monte-Carlo Ising Model (%s)\n", GIT_HASH);
  printf("# Copyright (C) 2022 True Merrill\n");
  printf("#\n");
  printf("# Arguments:\n");
  printf("#   --num-rows=%zu\n", S.nrows);
  printf("#   --num-cols=%zu\n", S.ncols);
  printf("#   --coupling=%f\n", S.J);
  printf("#   --magnetization=%f\n", S.H);
  printf("#   --beta=%f\n", S.beta);
  printf("#   --num-steps=%zu\n", S.nsteps);
  printf("#   --sample-start=%zu\n", S.nstart);
  printf("#   --sample-interval=%zu\n", S.nsample);
  printf("#\n");
}


void print_version(void)
{
  printf("%s\n", GIT_HASH);
}


void print_data(const ising_t *I)
{
  double mean_spin = get_mean_spin(I);
  double mean_energy = get_mean_energy(I);

  printf("% 8i, % 8.8f, % 8.8f, % 8.8f, % 8.8f, % 8.8f, % 8.8f, % 8.8f\n",
    I->t,
    I->J,
    I->H,
    I->beta,
    mean_spin,
    get_spin_variance(I, mean_spin),
    mean_energy,
    get_energy_variance(I, mean_energy)
  );
}


void print_data_header(void)
{
  printf("# %6s, %11s, %11s, %11s, %11s, %11s, %11s, %11s\n", 
    "step", "J", "h", "beta", 
    "< S >", "Var(S)", 
    "< E >", "Var(E)");
}


int main(int argc, char *argv[]) 
{
  parse(argc, argv);

  if (show_help) {
    print_help();
    return 0;
  }

  if (show_version) {
    print_version();
    return 0;
  }

  if (show_header) {
    print_header();
    print_data_header();
  }

  srand(time(NULL));
  ising_t *I = ising_alloc(S.nrows, S.ncols);
  set_random_spins(I);
  set_coupling(I, S.J);
  set_magnetization(I, S.H);
  set_beta(I, S.beta);

  for (size_t n = 0; n < S.nsteps; n++) {
    if (((I->t % S.nsample) == 0) && (I->t >= S.nstart)) {
      print_data(I);
    }
    metropolis_step(I);
  }

  if (I->t >= S.nstart) {
    print_data(I);
  }

  ising_free(I);
  return 0;
}
