#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "ising.h"



static inline size_t wrap(int n, size_t size)
{
  return n - floor(((double) n) / size) * size;
}


static inline size_t address(const ising_t *I, int n, int m)
{
  return  I->ncols * wrap(n, I->nrows) + wrap(m, I->ncols);
}


#define ADDR(I, n, m) ((I)->ncols * (n) + (m))


static void ising_calc_neighbor_addresses(ising_t *I)
{
  for (size_t n = 0; n < I->nrows; n++) {
    for (size_t m = 0; m < I->ncols; m++) {
      size_t idx = address(I, n, m);
      I->addr[idx].nup = address(I, n - 1, m);
      I->addr[idx].ndown = address(I, n + 1, m);
      I->addr[idx].nleft = address(I, n, m - 1);
      I->addr[idx].nright = address(I, n, m + 1);
    }
  }
}


ising_t *ising_alloc(size_t nrows, size_t ncols)
{
  ising_t *I = calloc(1, sizeof(ising_t));
  if (!I) {
    ising_free(I);
    return NULL;
  }

  I->S = calloc(nrows * ncols, sizeof(int));
  if (!(I->S)) {
    ising_free(I);
    return NULL;
  }

  I->addr = calloc(nrows * ncols, sizeof(ising_neighbors_t));
  if (!(I->addr)) {
    ising_free(I);
    return NULL;
  }

  I->nrows = nrows;
  I->ncols = ncols;
  I->J = 0.0;
  I->H = 0.0;
  I->beta = 0.0;
  ising_calc_neighbor_addresses(I);
  set_spins(I, 1);

  return I;
}


void ising_free(ising_t *I)
{
  if (I) {
    if (I->S) {
      free(I->S);
      I->S = NULL;
    }

    if (I->addr) {
      free(I->addr);
      I->addr = NULL;
    }

    I->nrows = 0;
    I->ncols = 0;
    I->J = 0.0;
    I->H = 0.0;
    I->beta = 0.0;
    I->t = 0;
    free(I);
  }
}


void set_coupling(ising_t *I, double J)
{
  I->J = J;
}


void set_magnetization(ising_t *I, double H)
{
  I->H = H;
}


void set_beta(ising_t *I, double beta)
{
  if (beta >= 0) {
    I->beta = beta;
  }
}


void set_temperature(ising_t *I, double kT)
{
  set_beta(I, 1 / kT);
}


static int random_spin(void)
{
  return 2 * (rand() % 2) - 1;
}


void set_random_spins(ising_t *I)
{
  size_t n = I->ncols * I->nrows;
  for (size_t i = 0; i < n; i++) {
    I->S[i] = random_spin();
  }
  I->t = 0;
}


void set_spins(ising_t *I, int value)
{
  int s = (value > 0) ? 1 : -1;
  size_t n = I->ncols * I->nrows;
  for (size_t i = 0; i < n; i++) {
    I->S[i] = s;
  }
  I->t = 0;
}


double get_spin(const ising_t *I, int n, int m)
{
  return (double) I->S[ADDR(I, n, m)];
}


double get_spin_coupling_energy(const ising_t *I, int n, int m)
{
  size_t idx = ADDR(I, n, m);
  const ising_neighbors_t *neighbors = (I->addr + idx);
  return - I->J * I->S[idx] * (
    I->S[neighbors->nup] + \
    I->S[neighbors->ndown] + \
    I->S[neighbors->nleft] + \
    I->S[neighbors->nright]
  );
}


double get_spin_magnetization_energy(const ising_t *I, int n, int m)
{
  size_t idx = ADDR(I, n, m);
  return - I->H * I->S[idx];
}


double get_spin_energy(const ising_t *I, int n, int m)
{
  return get_spin_magnetization_energy(I, n, m) + \
    get_spin_coupling_energy(I, n, m);
}


double get_mean_spin(const ising_t *I)
{
  double mean_spin = 0.0;
  size_t nspins = I->ncols * I->nrows;

  for (size_t n = 0; n < I->nrows; n++) {
    for (size_t m = 0; m < I->ncols; m++) {
      mean_spin += get_spin(I, n, m) / nspins;
    }
  }

  return mean_spin;
}


double get_spin_variance(const ising_t *I, double mean_spin)
{
  return 1.0 - pow(mean_spin, 2);
}


double get_mean_energy(const ising_t *I)
{
  double mean_energy = 0.0;
  size_t nspins = I->ncols * I->nrows;

  for (size_t n = 0; n < I->nrows; n++) {
    for (size_t m = 0; m < I->ncols; m++) {
      mean_energy += get_spin_energy(I, n, m) / nspins;
    }
  }

  return mean_energy;
}


double get_energy_variance(const ising_t *I, double mean_energy)
{
  double mean_energy_squared = 0;
  size_t nspins = I->ncols * I->nrows;

  for (size_t n = 0; n < I->nrows; n++) {
    for (size_t m = 0; m < I->ncols; m++) {
      mean_energy_squared += pow(get_spin_energy(I, n, m), 2) / nspins;
    }
  }

  return mean_energy_squared - pow(mean_energy, 2);
}


static void flip_spin(ising_t *I, int n, int m)
{
  size_t idx = ADDR(I, n, m);
  I->S[idx] *= -1;
}


static inline double random_uniform(void)
{
  return ((double) rand()) / RAND_MAX;
}


static void metropolis_trial(ising_t *I, int n, int m)
{
  double deltaE = - 2 * get_spin_energy(I, n, m);
  if (deltaE <= 0) {
    flip_spin(I, n, m);
  } else if (exp( - I->beta * deltaE) > random_uniform()) {
    flip_spin(I, n, m);
  }
}


void metropolis_step(ising_t *I)
{
  size_t nspins = I->ncols * I->nrows;
  for (size_t i = 0; i < nspins; i++) {
    size_t n = rand() % I->nrows;
    size_t m = rand() % I->ncols;
    metropolis_trial(I, n, m);
  }

  I->t += 1;
}
