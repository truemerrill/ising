# Metropolis Monte-Carlo Ising Model

Simulate a two-dimensional Ising model using the Metropolis algorithm.

```
ISING - Metropolis Monte-Carlo Ising Model (28b700a)

Usage: ising [arguments]               run simulation
   or: ising [arguments] > [outfile]   redirect output to file

Arguments:
   --num-rows=[n]         Number of rows in the Ising model
   --num-cols=[m]         Number of columns in the Ising model
   --coupling=[J]         Spin-spin coupling rate
   --magnetization=[h]    Magnetic field coupling rate
   --beta=[b]             Inverse thermodynamic temperature
   --num-steps=[N]        Number of Metropolis steps to simulate
   --sample-start=[Ns]    Step which to start logging data
   --sample-interval=[Ni] Number of steps between data logging
   --no-header            Do not print header in output
   --help                 Print help (this message) and exit
   --version              Print version information and exit
```

## Downloads

Please view the [releases page](https://gitlab.com/truemerrill/ising/-/releases).

## Build and Install

The software is implemented in C and uses `getopt` for argument
parsing.  Getopt is part of the standard Unix C distribution but
may not be present on some Windows C distributions.  We use the
`cmake` build system.  On a Unix system, execute the following
terminal commands from the repository root directory: 

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## History

Over a decade ago in my graduate statistical mechanics class I had
an assignment to simulate a 2D Ising model.  At the time I was 
green and only knew a smattering of Matlab.  I pulled and modified
some code from the internet and cludged together something that
passed.

But in the years since I've learned lots of programming languages
and developed extensive experience building complex software.  Each
time I learn a new language, I repeat the exercise of simulating a
2D Ising model as practice for myself.  The Ising model has many
features which make it a good practice problem for numerical
computing:

- 2D arrays, array indexing, and toroidal boundary conditions
- Mutable and immutable state
- Random numbers, with both uniform and exponential distributions
- Program IO and argument parsing
- An analytic solution to compare simulation results

Generally, if you have reached the basic level of skill to be able
to write an Ising model in a new language, you probably are able to
start working on a more complex numerical computing project.

Although I already know C/C++ well, I wrote this code for a past
version of myself: a graduate student with limited programing
experience who needs to write an Ising model.  

If you are reading this far, I hope youfind this code useful.  Feel
free to pull and modify this code from the internet and cludge
together something that passes.  But more importantly, in the future
go back and re-implement this code in your own langaug of choice.
Use this as a practice problem to test your own skills.
